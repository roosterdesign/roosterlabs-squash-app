import React, {Component} from 'react';
import DatePicker from 'react-datepicker';
import moment from 'moment';

export default class ScoreSingle extends Component {
	constructor(props) {
	    super(props);
		this.state = {
			startDate: moment(this.props.score.date),
			shouldHide: true
		};
	}

	handleChange(date) {
		this.setState({
			startDate: date
		})
	}

	updateScore() {
		this.setState({
			shouldHide: !this.state.shouldHide
		})
	}

	saveUpdateScore() {

		this.setState({
			shouldHide: !this.state.shouldHide
		})

		let date = moment(this.state.startDate).toDate(),
 			jamesscore = parseInt(this.refs.jamesscore.value.trim()),
 			neilscore = parseInt(this.refs.neilscore.value.trim());

		let formData = {
 			date: date,
 			jamesscore: jamesscore,
 			neilscore: neilscore
 		}

 		// Validation
 		if(!date) {
        	Bert.alert('Please enter a date', 'danger', 'fixed-top', 'fa-frown-o');
        } else if(this.refs.jamesscore.value.length == 0) {
        	Bert.alert('Please enter James score', 'danger', 'fixed-top', 'fa-frown-o');
        } else if(this.refs.neilscore.value.length == 0) {
        	Bert.alert('Please enter Neils score', 'danger', 'fixed-top', 'fa-frown-o');
        } else {
			Meteor.call('updateScore', formData, this.props.score, (error, data)=>{
				if(error) {
	        		Bert.alert('There has been an error updating the score.', 'danger', 'fixed-top', 'fa-frown-o');
	        	} else {
	        		Bert.alert('Score updated.', 'success', 'fixed-top', 'fa-smile-o');
	        	}
	        });
        }

	}

	deleteScore() {
		Meteor.call('deleteScore', this.props.score);
	}


	render() {
		let date = moment(this.props.score.date).format('DD/MM/YYYY');
		let username = Meteor.user().username;
		let actionButton;
		if (username === "admin") {
		  actionButton = <ActionButtons />;
		}

		return(
			<tr className={this.state.shouldHide ? 'row' : 'row editable'}>
				<td>
					<span className="readState">{date}</span>
					<DatePicker dateFormat="DD/MM/YYYY" showTodayButton={'Today'} id="date" className="form-control editState" selected={this.state.startDate} onChange={this.handleChange.bind(this)} placeholderText="Select a date" />
				</td>
				<td className="text-center">
					<span className="readState">{this.props.score.neilscore}</span>
					<input
						type="number"
						className="form-control editState"
						ref="neilscore"
						min="0"
						defaultValue={this.props.score.neilscore} />
				</td>
				<td className="text-center">
					<span className="readState">{this.props.score.jamesscore}</span>
					<input
						type="number"
						className="form-control editState"
						ref="jamesscore"
						min="0"
						defaultValue={this.props.score.jamesscore} />
				</td>
				<td className="text-center">
					<span className="readState">{this.props.score.winner}</span>
					<span className="editState">-</span>
				</td>
				<td className="text-right">
					

					<button className="btn btn-primary readState" onClick={this.updateScore.bind(this)} >
						<i className="fa fa-pencil" aria-hidden="true"></i>
					</button>
					<button className="btn btn-danger readState" onClick={this.deleteScore.bind(this)} >
						<i className="fa fa-times" aria-hidden="true"></i>
					</button>
					<button className="btn btn-success editState" onClick={this.saveUpdateScore.bind(this)} >
						<i className="fa fa-check" aria-hidden="true"></i> Update
					</button>
		

				</td>
			</tr>
		)
	}
}