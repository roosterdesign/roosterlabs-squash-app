import React, {Component} from 'react';
import TrackerReact from 'meteor/ultimatejs:tracker-react';

import ScoreSingle from './ScoreSingle.jsx';
import AddScore from './AddScore.jsx';
import ScoreStats from './ScoreStats.jsx';

Scores = new Mongo.Collection("scores");

export default class ScoresWrapper extends TrackerReact(Component) {
	constructor() {
		super();

		this.state = {
			subscription: {
				scores: Meteor.subscribe('allScores'),
			}
		}
	}

	componentWillUnmount() {
		this.state.subscription.scores.stop();
	}

	scores() {
        return Scores.find({}, {sort: {date: 1}}).fetch();
    }

	render() {
		return(
			<div className="row">

				<div className="col-sm-9 scores">

					<AddScore />	

					<table className="table table-striped">
						<thead>
							<tr className="row">
								<th className="col-sm-3">Date</th>
								<th className="col-sm-2 text-center">Neil</th>
								<th className="col-sm-2 text-center">James</th>
								<th className="col-sm-2 text-center">Winner</th>
								<th className="col-sm-3 text-right">Actions</th>
							</tr>
						</thead>
						<tbody>
							{this.scores().map( (score)=> {
								return <ScoreSingle key={score._id} score={score} />
							})}					
						</tbody>
					</table>
				</div>

				<aside className="col-sm-3">
					<ScoreStats />
				</aside>

			</div>
		)
	}
}