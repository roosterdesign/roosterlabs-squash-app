import React, {Component} from 'react';

export default class ActionButtons extends Component {
	render() {
		return(
			<div>
				<button className="btn btn-primary readState" onClick={this.updateScore.bind(this)} >
					<i className="fa fa-pencil" aria-hidden="true"></i>
				</button>
				<button className="btn btn-danger readState" onClick={this.deleteScore.bind(this)} >
					<i className="fa fa-times" aria-hidden="true"></i>
				</button>
				<button className="btn btn-success editState" onClick={this.saveUpdateScore.bind(this)} >
					<i className="fa fa-check" aria-hidden="true"></i> Update
				</button>
			</div>
		)
	}
}