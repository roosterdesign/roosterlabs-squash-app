import React, {Component} from 'react';
import TrackerReact from 'meteor/ultimatejs:tracker-react';

export default class ScoreStats extends TrackerReact(Component) {

	constructor() {
		super();

		this.state = {
			subscription: {
				scores: Meteor.subscribe('allScores')
			}
		}
	}

	componentWillUnmount() {
		this.state.subscription.scores.stop();
	}

     matchScores() {

    	let jamesWins = Scores.find({winner: "James"}).fetch().length
    		neilWins = Scores.find({winner: "Neil"}).fetch().length
    		draws = Scores.find({winner: "Draw"}).fetch().length;

    	let results = {
    		jamesWins: jamesWins,
    		neilWins: neilWins,
    		draws: draws,

    		overallGames: function() { return jamesWins + neilWins + draws },

    		leader: function() {

			    		if (jamesWins > neilWins) { return "James" }
			    		else if  (jamesWins < neilWins) { return "Neil" }
		    			else if  (jamesWins == neilWins) { return "Drawing" }
			    		//else { return "Drawing" }
					}
		}
		return results
	}

	render() {
		return(
			<div>
				<div className="well stats">
					<h2>Stat Attack</h2>
					<hr />

					<div className="row">
						<div className="col-sm-8"><strong>Current leader:</strong></div>
						<div className="col-sm-4">{this.matchScores().leader()}</div>
					</div>

					<div className="row">
						<div className="col-sm-8"><strong>Neil wins:</strong></div>
						<div className="col-sm-4">{this.matchScores().neilWins}</div>
					</div>

					<div className="row">
						<div className="col-sm-8"><strong>James wins:</strong></div>
						<div className="col-sm-4">{this.matchScores().jamesWins}</div>
					</div>

					<div className="row">
						<div className="col-sm-8"><strong>Draws:</strong></div>
						<div className="col-sm-4">{this.matchScores().draws}</div>
					</div>

					<div className="row">
						<div className="col-sm-8"><strong>Total games:</strong></div>
						<div className="col-sm-4">{this.matchScores().overallGames()}</div>
					</div>

				</div>
			</div>
		)
	}
}