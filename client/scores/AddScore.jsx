import React, {Component} from 'react';
import DatePicker from 'react-datepicker';
import moment from 'moment';

export default class AddScore extends Component {
	constructor(props) {
	    super(props);
		this.state = {startDate: moment()};
	}

	handleChange(date) {
		this.setState({
			startDate: date
		})
	}

 	addScore(event) {
 		event.preventDefault();

 		let date = moment(this.state.startDate).toDate(),
 			neilscore = parseInt(this.refs.neilscore.value.trim()),
 			jamesscore = parseInt(this.refs.jamesscore.value.trim());

 		let formData = {
 			date: date,
 			neilscore: neilscore,
 			jamesscore: jamesscore 			
 		}

 		// Validation
 		if(!date) {
        	Bert.alert('Please enter a date', 'danger', 'fixed-top', 'fa-frown-o');
        } else if(this.refs.neilscore.value.length == 0) {
        	Bert.alert('Please enter Neils score', 'danger', 'fixed-top', 'fa-frown-o');
        } else if(this.refs.jamesscore.value.length == 0) {
        	Bert.alert('Please enter James score', 'danger', 'fixed-top', 'fa-frown-o');
        } else {

        	// Call Meteor add method
        	Meteor.call('addScore', formData, (error, data)=>{
	        	if(error) {

	        		// Error message
	        		Bert.alert('Please login before submitting', 'danger', 'fixed-top', 'fa-frown-o');

	        	} else {

	        		// Success message
	        		Bert.alert('Score added.', 'success', 'fixed-top', 'fa-smile-o');

	        		// Reset fields
	        		this.setState({startDate: moment()});
	        		this.refs.jamesscore.value = "";
	        		this.refs.neilscore.value = "";
	        		this.refs.jamesscore.focus();
	        	}
	        });
        }

 	}

 	render() {
 		return(
 			<div>
 				<form className="add-score" onSubmit={this.addScore.bind(this)}>

 					<div className="well">

	 					<div className="row">
	 						<div className="col-sm-2">
	 							<DatePicker
			 						dateFormat="DD/MM/YYYY"
			 						showTodayButton={'Today'}
			 						id="date"
			 						className="form-control"
							        selected={this.state.startDate}
							        onChange={this.handleChange.bind(this)}
							        placeholderText="Select a date" />
	 						</div>
	 						<div className="col-sm-4">
	 							<input
			 						type="number"
			 						className="form-control"
			 						ref="neilscore"
			 						min="0"
			 						placeholder="Neil Score" />								
	 						</div>
	 						<div className="col-sm-4">
	 							<input
			 						type="number"
			 						className="form-control"
			 						ref="jamesscore"
			 						min="0"
			 						placeholder="James Score" />
								
	 						</div>
	 						<div className="col-sm-2">
	 							<input
			 						type="submit"
			 						className="btn btn-primary"
			 						value="Add Score" />
	 						</div>
	 					</div>

	 				</div>
 					
 				</form>
 			</div>
 		)
 	}
 }