import React from 'react';
import AccountsUI from '../accounts/AccountsUI.jsx';

Accounts.ui.config({
  passwordSignupFields: 'USERNAME_ONLY',
});

export const MainLayout = ({content}) => (
	<div className="main-layout">
		<header>
			<div className="container">
				<div className="row">
					<div className="col-sm-6">
						<h1>JB&G Squash</h1>
					</div>
					<div className="col-sm-6 text-right">
						<AccountsUI />						
					</div>
				</div>
			</div>
		</header>
		<main className="container">
			{content}
		</main>
	</div>
)