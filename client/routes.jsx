import React from 'react';
import {mount} from 'react-mounter';

import {MainLayout} from './layouts/MainLayout.jsx';
import ScoresWrapper  from './scores/ScoresWrapper.jsx';

FlowRouter.route('/', {
	action() {
		mount(MainLayout, {
			content: (<ScoresWrapper />)
		})
	}
});