Meteor.methods({

	// Add Score
	addScore(formData) {

		//if(!Meteor.userId()) {
		if( Meteor.user().username !== "admin" ) {
			throw new Meteor.Error('not-authorised');
		}

		check(formData, {
		    date: Date,
		    jamesscore: Number,
		    neilscore: Number
		});

		if (formData.jamesscore > formData.neilscore) {
			var winner = 'James'
		} else if (formData.jamesscore < formData.neilscore) {
			var winner = 'Neil'
		} else {
			var winner = 'Draw'
		};

		Scores.insert({
			date: formData.date,
            jamesscore: formData.jamesscore,
            neilscore: formData.neilscore,
            winner: winner,
            createdAt: new Date()
        });
	},


	// Delete Score
	deleteScore(score) {
		if( Meteor.user().username !== "admin" ) {
			throw new Meteor.Error('not-authorised');
		}
		check(score, Object);
		Scores.remove(score._id);
	},


	// Update Scores
	updateScore(formData,score) {
		if( Meteor.user().username !== "admin" ) {
			throw new Meteor.Error('not-authorised');
		}
		check(formData, {
		    date: Date,
		    jamesscore: Number,
		    neilscore: Number
		});

		if (formData.jamesscore > formData.neilscore) {
			var winner = 'James'
		} else if (formData.jamesscore < formData.neilscore) {
			var winner = 'Neil'
		} else {
			var winner = 'Draw'
		};

        Scores.update(score._id, {
        	$set: {
				date: formData.date,
	            jamesscore: formData.jamesscore,
	            neilscore: formData.neilscore,
				winner: winner
			}
		});

	}

});